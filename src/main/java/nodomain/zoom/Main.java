package nodomain.zoom;

import org.lwjgl.input.Keyboard;

import net.fabricmc.api.ModInitializer;
import net.legacyfabric.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.option.KeyBinding;
import nodomain.zoom.config.Config;

public class Main implements ModInitializer {
	@Override
	public void onInitialize() {
		Config.load();

		Config.zoomBind = KeyBindingHelper.registerKeyBinding(new KeyBinding(
			"key.zoom.zoom",
			Keyboard.KEY_C,
			"key.categories.misc"
		));
	}
}
