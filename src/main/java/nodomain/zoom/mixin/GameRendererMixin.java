package nodomain.zoom.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.GameRenderer;
import nodomain.zoom.config.Config;
import nodomain.zoom.config.SmoothingMode;

@Mixin(GameRenderer.class)
public class GameRendererMixin {
	@Unique
	private boolean prevSmoothCamera = false;

	@Shadow
	private float cursorDeltaX;
	@Shadow
    private float cursorDeltaY;
	@Shadow
    private float smoothedCursorDeltaX;
	@Shadow
    private float smoothedCursorDeltaY;

	@Inject(method = "getFov(FZ)F",
	at = @At(value = "RETURN", ordinal = 1),
	cancellable = true)
	public void changeFov(float tickDelta, boolean changingFov, CallbackInfoReturnable<Float> info) {
		if (Config.zoomBind.isPressed()) {
			Config.zoomEnd = 0;
			if (Config.zoomStart == 0) {
				Config.zoomStart = System.currentTimeMillis();

				prevSmoothCamera = MinecraftClient.getInstance().options.smoothCameraEnabled;
				if (Config.smoothCamera) {
					MinecraftClient.getInstance().options.smoothCameraEnabled = true;
					/* TODO: Fix, smooth camera still keeps mouse velocity */
					cursorDeltaX = 0;
					cursorDeltaY = 0;
					smoothedCursorDeltaX = 0;
					smoothedCursorDeltaY = 0;
				}
			}

			float progress = (System.currentTimeMillis() - Config.zoomStart) / (float)(Config.zoomInLength*1000);
			progress = smooth(progress, Config.zoomInSmoothing);

			float originalFov = info.getReturnValueF();
			float modifiedFov = originalFov - (progress * Config.zoomAmount);
			if (modifiedFov < 0.1f) modifiedFov = 0.1f;

			info.setReturnValue(modifiedFov);
		} else {
			Config.zoomStart = 0;
			if (Config.zoomEnd == 0) {
				Config.zoomEnd = System.currentTimeMillis();
				if (Config.smoothCamera)
					MinecraftClient.getInstance().options.smoothCameraEnabled = prevSmoothCamera;
			}

			float progress = (System.currentTimeMillis() - Config.zoomEnd) / (float)(Config.zoomOutLength*1000);
			progress = smooth(progress, Config.zoomOutSmoothing);
			if (progress > 1f) return;

			float originalFov = info.getReturnValueF();
			float modifiedFov = originalFov - Config.zoomAmount + (progress * Config.zoomAmount);
			if (modifiedFov < 0.1f) modifiedFov = 0.1f;

			info.setReturnValue(modifiedFov);
		}
	}

	@Unique
	private float smooth(float progress, SmoothingMode smoothing) {
		if (progress >= 1f) return 1f;

		if (smoothing.equals(SmoothingMode.CUBIC_EASE_IN))
			progress = (float)Math.pow(progress, 3);
		else if (smoothing.equals(SmoothingMode.CUBIC_EASE_OUT))
			progress = 1f - (float)Math.pow(1f - progress, 3);
		else if (smoothing.equals(SmoothingMode.CUBIC_EASE_IN_OUT)) {
			if (progress < 0.5f)
				progress = (float)Math.pow(progress * 2, 3) / 2f;
			else
				progress = 1f - (float)Math.pow(2f * (1f - progress), 3) / 2f;
		}
		else if (smoothing.equals(SmoothingMode.CUBIC_EASE_OUT)
			|| (smoothing.equals(SmoothingMode.CUBIC_EASE_IN_OUT)))
			progress = 1f - (float)Math.pow(2f * (1f - progress), 3) / 2f;
		else if (smoothing.equals(SmoothingMode.QUADRATIC_EASE_OUT))
			progress = -(float)Math.pow(progress - 1, 2) + 1;
		else if (smoothing.equals(SmoothingMode.CIRCULAR_EASE_OUT))
			progress = (float)Math.sqrt(1f - Math.pow(progress-1f, 2));
		else if (smoothing.equals(SmoothingMode.EXPONENTIAL_EASE_OUT))
			progress = -(float)Math.pow(2, -10 * progress) + 1;

		if (progress < 0f) progress = 0f;
		if (progress > 1f) progress = 1f;
		return progress;
	}
}
