package nodomain.zoom.config;

public enum SmoothingMode {
	LINEAR("Linear"),
	CUBIC_EASE_IN("Cubic Ease-In"),
	CUBIC_EASE_OUT("Cubic Ease-Out"),
	CUBIC_EASE_IN_OUT("Cubic Ease-In-Out"),
	QUADRATIC_EASE_OUT("Quadratic Ease-Out"),
	CIRCULAR_EASE_OUT("Circular Ease-Out"),
	EXPONENTIAL_EASE_OUT("Exponential Ease-Out");

	private String displayText;

	SmoothingMode(String displayText) {
		this.displayText = displayText;
	}

	public String getText() {
		return this.displayText;
	}
}
