package nodomain.zoom.config;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import net.minecraft.client.option.KeyBinding;

public class Config {
	public static transient final Gson gson = new GsonBuilder()
		.excludeFieldsWithModifiers(java.lang.reflect.Modifier.TRANSIENT)
		.setPrettyPrinting()
		.create();
	public static transient final File file = new File("config/Zoom.json");
	public static transient KeyBinding zoomBind;
	public static transient long zoomStart = 0;
	public static transient long zoomEnd = 0;
	public static float zoomAmount = 50f;
	public static SmoothingMode zoomInSmoothing = SmoothingMode.EXPONENTIAL_EASE_OUT;
	public static SmoothingMode zoomOutSmoothing = SmoothingMode.EXPONENTIAL_EASE_OUT;
	public static double zoomInLength = 0.5;
	public static double zoomOutLength = 0.25;
	public static boolean smoothCamera = true;

	public static void save() {
		Config.file.getParentFile().mkdirs();
		try {
			Config.file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		try (Writer configWriter = new FileWriter(Config.file)) {
			Config.gson.toJson(new Config(), configWriter);
		} catch (JsonIOException | IOException e) {
			e.printStackTrace();
		}
	}

	public static void load() {
		try (JsonReader configReader = new JsonReader(new FileReader(Config.file))) {
			gson.fromJson(configReader, Config.class);
		} catch (JsonIOException | JsonSyntaxException e) {
			e.printStackTrace();
			Config.save(); /* Regenerate from whatever config we have in memory */
		} catch (IOException e) { /* Config probably hasn't been saved yet */ }
	}
}
