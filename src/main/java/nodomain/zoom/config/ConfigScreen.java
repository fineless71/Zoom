package nodomain.zoom.config;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.resource.language.I18n;

public class ConfigScreen extends Screen {
	private Screen parent = null;

	public ConfigScreen(Screen parent) {
		this.parent = parent;
		Config.load();
	}

	@Override
	public void init() {
		this.buttons.add(new ButtonWidget(1, this.width / 2 - 100, 10, 20, 20, "<"));
		this.buttons.add(new ButtonWidget(2, this.width / 2 + 80, 10, 20, 20, ">"));
		this.buttons.add(new ButtonWidget(3, this.width / 2 - 100, 40, "Zoom In Smoothing: " + Config.zoomInSmoothing.getText()));
		this.buttons.add(new ButtonWidget(4, this.width / 2 - 100, 70, 20, 20, "<"));
		this.buttons.add(new ButtonWidget(5, this.width / 2 + 80, 70, 20, 20, ">"));
		this.buttons.add(new ButtonWidget(6, this.width / 2 - 100, 100, "Zoom Out Smoothing: " + Config.zoomOutSmoothing.getText()));
		this.buttons.add(new ButtonWidget(7, this.width / 2 - 100, 130, 20, 20, "<"));
		this.buttons.add(new ButtonWidget(8, this.width / 2 + 80, 130, 20, 20, ">"));
		this.buttons.add(new ButtonWidget(9, this.width / 2 - 100, 160, "Smooth Camera: " + (Config.smoothCamera ? "ON" : "OFF")));
		this.buttons.add(new ButtonWidget(0, this.width-200-10, this.height-20-10, I18n.translate("gui.done")));
    }

	@Override
	protected void buttonClicked(ButtonWidget button) {
		if (button.id == 1) {
			Config.zoomAmount -= 5f;
		}

		if (button.id == 2) {
			Config.zoomAmount += 5f;
		}

		if (button.id == 3) {
			Config.zoomInSmoothing = Config.zoomInSmoothing.values()[(Config.zoomInSmoothing.ordinal() + 1) % Config.zoomInSmoothing.values().length];
			button.message = "Zoom In Smoothing: " + Config.zoomInSmoothing.getText();
		}

		if (button.id == 4) {
			if (Config.zoomInLength > 1.01) Config.zoomInLength -= 0.5f;
			else Config.zoomInLength -= 0.05f;
		}

		if (button.id == 5) {
			if (Config.zoomInLength >= 0.99) Config.zoomInLength += 0.5f;
			else Config.zoomInLength += 0.05f;
		}

		if (button.id == 6) {
			Config.zoomOutSmoothing = Config.zoomOutSmoothing.values()[(Config.zoomOutSmoothing.ordinal() + 1) % Config.zoomOutSmoothing.values().length];
			button.message = "Zoom Out Smoothing: " + Config.zoomOutSmoothing.getText();
		}

		if (button.id == 7) {
			if (Config.zoomOutLength > 1.01) Config.zoomOutLength -= 0.5f;
			else Config.zoomOutLength -= 0.05f;
		}

		if (button.id == 8) {
			if (Config.zoomOutLength >= 0.99) Config.zoomOutLength += 0.5f;
			else Config.zoomOutLength += 0.05f;
		}

		if (button.id == 9) {
			Config.smoothCamera = !Config.smoothCamera;
			button.message = "Smooth Camera: " + (Config.smoothCamera ? "ON" : "OFF");
		}

		if (Config.zoomAmount < 20f) Config.zoomAmount = 20f;
		if (Config.zoomAmount > 110f) Config.zoomAmount = 110f;

		if (Config.zoomInLength > 3.0) Config.zoomInLength = 3.0;
		if (Config.zoomInLength < 0) Config.zoomInLength = 0;

		if (Config.zoomOutLength > 3.0) Config.zoomOutLength = 3.0;
		if (Config.zoomOutLength < 0) Config.zoomOutLength = 0;

		if (button.id == 0) {
			this.client.setScreen(this.parent);
		}
    }

	@Override
    public void render(int mouseX, int mouseY, float tickDelta) {
        this.renderBackground();
		this.drawCenteredString(this.textRenderer, "Zoom Amount: " + (int)Math.round(Config.zoomAmount), this.width / 2, 20 - (textRenderer.fontHeight/2), 0xFFFFFF);
		this.drawCenteredString(this.textRenderer, "Zoom In Length: " + roundTo(Config.zoomInLength, 2) + " seconds", this.width / 2, 80 - (textRenderer.fontHeight/2), 0xFFFFFF);
		this.drawCenteredString(this.textRenderer, "Zoom Out Length: " + roundTo(Config.zoomOutLength, 2) + " seconds", this.width / 2, 140 - (textRenderer.fontHeight/2), 0xFFFFFF);
        super.render(mouseX, mouseY, tickDelta);
    }

	@Override
	public void removed() {
		Config.save();
    }

	private double roundTo(double value, int precision) {
		int scale = (int) Math.pow(10, precision);
		return (double) Math.round(value * scale) / scale;
	}
}
