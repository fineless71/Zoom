# Zoom

For Legacy Fabric 1.7.x to 1.8.x. This mod allows you to zoom in while holding a key.

![Screenshot](screenshot.gif)

## Dependencies

[Legacy Fabric API](https://github.com/Legacy-Fabric/fabric/releases)

TODO: Add scrolling support

TODO: Allow to not need a key press and just overwrite scroll all together

TODO: Make it smoother when cancelling a zoom. Maybe set the start progress to 1-prev?
